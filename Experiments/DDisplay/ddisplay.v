module ddisplay(seg,sel,clk,Data0,Data1,Data2,Data3);
input clk;
input[3:0] Data0,Data1,Data2,Data3;
output reg[1:0] sel;
output[6:0] seg;
wire[3:0] tempData;
	
	//sel4_1(clk,D0,D1,D2,D3,sel,tempData);
	sel4_1 mysel4_1(clk,Data0,Data1,Data2,Data3,sel,tempData);

	//decode4_7(a,b,c,d,e,f,g,data)
	decode4_7 mydec(seg[0],seg[1],seg[2],seg[3],seg[4],seg[5],seg[6],tempData);	
endmodule
