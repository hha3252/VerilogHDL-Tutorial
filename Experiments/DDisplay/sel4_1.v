module sel4_1(clk,D0,D1,D2,D3,sel,tempData);
input clk;
input[3:0] D0,D1,D2,D3;
output reg[1:0] sel;
output reg[3:0] tempData;
always @(posedge clk)
	begin
		case(sel)
			0:tempData=D0;
			1:tempData=D1;
			2:tempData=D2;
			3:tempData=D3;
			default:tempData=D0;
		endcase
		sel=sel+1;
	end
endmodule
