module bcdAdder(a,b,cin,ShiWei,GeWei);
input[3:0] a,b;
input cin;
output reg[3:0] ShiWei,GeWei;
reg[4:0] temp;
always @(a,b,cin)
	begin
		temp = a+b+cin;
		ShiWei = temp/10;
		GeWei = temp%10;
		//if (temp>9) {ShiWei,GeWei}=temp+6;
		//else {ShiWei,GeWei}=temp;
	end
endmodule
