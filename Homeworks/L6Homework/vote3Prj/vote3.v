//用case语句实现一个三人表决器
module  vote3(a,b,c,pass);
input a,b,c;
output reg pass;
always @(a,b,c)
   begin
      case({a,b,c})
          3'b000 :  pass=1'b0;   //0表示不通过
          3'b001 :  pass=1'b0;
          3'b010 :  pass=1'b0;
          3'b011 :  pass=1'b1;  //1表示通过
          3'b100 :  pass=1'b0;
          3'b101 :  pass=1'b1;
          3'b110 :  pass=1'b1;
          3'b111 :  pass=1'b1;
          default :  pass=1'b0;
      endcase
    end
endmodule

