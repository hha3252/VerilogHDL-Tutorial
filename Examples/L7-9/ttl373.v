//8位数据锁存器（74LS373）
module ttl373(le,oe,q,d);
input le,oe; input[7:0] d;
output reg[7:0] q;
always @*   //或写为always @(le,oe,d)
begin
    if(~oe & le) q<=d;
    //或写为if((!oe) && (le))
    else q<=8'bz;
end
endmodule
