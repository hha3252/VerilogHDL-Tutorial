//非流水线方式8位全加器
module adder8(cout,sum,ina,inb,cin,clk);
input[7:0] ina,inb; input cin,clk; output[7:0] sum; output cout;
reg[7:0] tempa,tempb,sum; reg cout,tempc;
always @(posedge clk)
begin   tempa=ina;tempb=inb;tempc=cin; end
//输入数据锁存
always @(posedge clk)
begin    {cout,sum}=tempa+tempb+tempc; end
endmodule
