//调用门元件bufif1描述的三态门
module tristate2(in,en,out);
input in,en;
output out;
tri out;
bufif1 b1(out,in,en);
//注意三态门端口的排列顺序
endmodule
