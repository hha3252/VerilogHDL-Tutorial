//三态双向总线缓冲器
module ttl245(a,b,oe,dir);
input oe,dir; 	//使能信号和方向控制
inout[7:0] a,b; 	//双向数据线
assign a=({oe,dir}==2'b00)?b:8'bz;
assign b=({oe,dir}==2'b01)?a:8'bz;
endmodule
