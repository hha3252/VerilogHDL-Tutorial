//电平敏感的1位数据锁存器
module latch1(q,d,le);
input d,le;
output q;
assign q=le?d:q;
//le为高电平时，将输入端数据锁存
endmodule
