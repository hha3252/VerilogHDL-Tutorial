//n为5反馈系数Ci分别为(45)8，(67)8，(75)8的m序列发生器
module m_seq5(clr,clk,sel,m_out);
input clr,clk; output reg m_out; reg[4:0] shift_reg;
input[1:0] sel;  			//设置端，用于选择反馈系数
always @(posedge clk or negedge clr)
begin  if(~clr) begin shift_reg<=0; end	//异步复位，低电平有效
    else  begin
        case (sel)
            2'b00: begin  			//反馈系数Ci为(45)8
                shift_reg[0]<=shift_reg[2] ^ shift_reg[4];
                shift_reg[4:1]<=shift_reg[3:0]; end
            2'b01: begin 			//反馈系数Ci为(67)8
                shift_reg[0]<=shift_reg[0] ^ shift_reg[2] ^ shift_reg[3]  ^ shift_reg[4];
                shift_reg[4:1]<=shift_reg[3:0]; end
            2'b10: begin  			//反馈系数Ci为(75)8
                shift_reg[0]<=shift_reg[0] ^ shift_reg[1] ^ shift_reg[2] ^ shift_reg[4];
                shift_reg[4:1]<=shift_reg[3:0]; end
            default: shift_reg<=5'bX;
        endcase
        m_out <= shift_reg[4];
    end   end
endmodule
