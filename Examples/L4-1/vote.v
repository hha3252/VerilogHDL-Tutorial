module vote(a,b,c,f);
input a,b,c;			//模块的输入端
output f;				//模块的输出端
wire a,b,c,f;           //定义信号的数据类
assign f = (a&b)|(a&c)|(b&c);	//逻辑功能描述
endmodule
