//同步置数、同步清零的计数器
module count(out,data,load,reset,clk);
output[7:0] out; input[7:0] data;
input load,clk,reset;  reg[7:0] out;
always @(posedge clk)      //clk上升沿触发
begin
    if(!reset) out=8'h00; //同步清0，低电平有效
    else  if(load) out=data; //同步预置
    else	 	out=out+1;	  //计数
end
endmodule
