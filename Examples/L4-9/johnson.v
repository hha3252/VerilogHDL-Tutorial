//Johnson计数器（异步复位）
module johnson(clk,clr,qout);
parameter WIDTH=4;
input clk,clr;
output reg[(WIDTH-1):0] qout;
always @(posedge clk or posedge clr)
begin
    if(clr)  qout<=0;
    else
    begin
        qout<=qout<<1;
        qout[0]<=~qout[WIDTH-1];
    end
end
endmodule
