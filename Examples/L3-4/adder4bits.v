module adder4bits(cout,sum,a,b,cin);
output cout;
output[3:0] sum;
input[3:0] a,b;
input cin;
	
	assign {cout,sum} = a+b+cin;

endmodule

