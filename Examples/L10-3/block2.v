//阻塞赋值方式描述的移位寄存器2。
module block2(q0,q1,q2,q3,din,clk);
input clk,din; output reg q0,q1,q2,q3;
always @(posedge clk)
begin  	q3=q2;
    q1=q0;
    //该句与下句的顺序与例10.9颠倒
    q2=q1;
    q0=din;
end  endmodule
