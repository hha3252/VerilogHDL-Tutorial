//4位二进制加法计数器
module count4(out,reset,clk);
input reset,clk; output reg[3:0] out;
always @(posedge clk)
begin
    if(reset) 	out<=0;  //同步复位
    else    	out<=out+1;	//计数
end
endmodule
