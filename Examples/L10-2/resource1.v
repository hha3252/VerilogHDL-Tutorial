// 2个加法器和1个选择器的实现方式
module resource1(sum,a,b,c,d,sel);
parameter SIZE=4; input sel;
input[SIZE-1:0] a,b,c,d;
output reg[SIZE:0] sum;
always @(*)           //使用通配符
begin if(sel) 	sum=a+b;
    else  	sum=c+d;
end   endmodule
