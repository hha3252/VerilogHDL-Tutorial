//流水线加法器
module adder8(cout,sum,a,b,cin,enable);
input[7:0] a,b;  input cin,enable;
output[7:0] sum; reg[7:0] sum;
output cout;reg cout;
reg[3:0] tempa,tempb,firsts; reg firstc;
always @(posedge enable)
begin
    {firstc,firsts}=a[3:0]+b[3:0]+cin;
    tempa=a[7:4];  tempb=b[7:4];
end
always @(posedge enable)
begin
    {cout,sum[7:4]}=tempa+tempb+firstc;
    sum[3:0]=firsts;
end
endmodule
