module mod5Cnt(clk,reset,car,out);
input clk,reset;
output car,out;
reg[2:0]cnt;
reg car;
wire[2:0]out;
assign out=cnt;
always@(posedge clk or negedge reset)
  begin
    if(!reset)
      begin
        cnt<=3'b000;
        car<=0;
      end
    else
      begin
        if(cnt==3'b100)
          begin
            car<=3'b001;
            cnt<=3'b000;
          end
        else
          begin
            cnt<=cnt+3'b001;
            car<=3'b000;
          end
      end
  end
endmodule
