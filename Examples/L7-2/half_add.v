//半加器定义
module half_add(a,b,so,co);
input a,b;
output so,co;
assign co=a&b;
assign so=a^b;
endmodule
