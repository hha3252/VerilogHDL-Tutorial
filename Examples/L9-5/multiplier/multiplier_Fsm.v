//利用状态机设计8位乘法器

module multiplier8Bits(clk,rst,a,b,R);
input clk,rst;
input[7:0] a,b;
output reg[15:0] R;
parameter S0=0,S1=1,S2=2;
reg[2:0] count;
reg[1:0] state;
reg[15:0] temp_R,temp_a;
reg[7:0] temp_b;

always @(posedge clk)
begin
    if (rst) state<=S0;
	 else
	 begin
	 case(state)
        S0:begin
		  count<=0;temp_R<=0;temp_a<=a;temp_b<=b;state<=S1;
           end
        S1:begin
					if(count==3'b111) state<=S2;
					else
						begin
							 if(temp_b[0]) begin temp_R<=temp_R+temp_a; end
							 else begin temp_R<=temp_R; end
							 temp_a<=temp_a<<1;
							 temp_b<=temp_b>>1;
							 count<=count+1;
							 state<=S1;
						end
           end
        S2:begin
            R<=temp_R;
				state<=S0;
           end
        default:state<=S0;
    endcase
	 end	
end

endmodule
