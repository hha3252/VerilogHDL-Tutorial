//占空比50%的奇数分频（模7）
module count7(reset,clk,cout);
input clk,reset; output wire cout;
reg[2:0] m,n; reg cout1,cout2;
assign cout=cout1|cout2;		//两个计数器的输出相或
always @(posedge clk)
begin
    if(!reset)  begin  cout1<=0;  m<=0;  end
    else  begin 	if(m==6)  m<=0; 	else  m<=m+1;
        if(m<3) cout1<=1;	else cout1<=0;  end
end
always @(negedge clk)
begin
    if(!reset) begin cout2<=0;  n<=0;  end
    else  begin 	if(n==6)  n<=0; 	else  n<=n+1;
        if(n<3) cout2<=1; 	else cout2<=0; end
end
endmodule
    /*
    占空比50%的奇数分频
    module count_num(reset,clk,cout);
    parameter NUM=13;
    input clk,reset; output wire cout;
    reg[4:0] m,n; reg cout1,cout2;
    assign cout=cout1|cout2;
    always @(posedge clk)
    begin if(!reset)  begin  cout1<=0;  m<=0;  end
    else
    begin 	if(m==NUM-1)  m<=0; 	else  m<=m+1;
    		if(m<(NUM-1)/2) cout1<=1; else cout1<=0;
    end
    end
    always @(negedge clk)
    begin 	if(!reset) begin cout2<=0;  n<=0;  end
    else  begin
    if(n==NUM-1)  n<=0; 	else  n<=n+1;
    	 	 if(n<(NUM-1)/2) cout2<=1; else cout2<=0;  end
    end
    endmodule

    */
