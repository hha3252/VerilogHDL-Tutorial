//8位乘法器的仿真
`timescale 10ns/1ns
module mult8_tp;     //测试模块的名字
reg[7:0] a,b;	           //测试输入信号定义为reg型
wire[15:0] out;         //测试输出信号定义为wire型
integer i,j;
mult8 m1(out,a,b);	//调用测试对象
initial 		           //激励波形设定
begin 	 a=0;b=0;
    for(i=1;i<255;i=i+1)  #10 a=i;  end
initial begin
    for(j=1;j<255;j=j+1)  #10 b=j;
end
initial begin	//定义结果显示格式
    $monitor($time,,,"%d*%d=%d",a,b,out);
    #2560 $finish;
end
endmodule
