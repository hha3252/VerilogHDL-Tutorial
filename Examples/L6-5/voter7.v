//用for语句描述七人投票表决器
module  voter7(pass,vote);
output  pass;
input[6:0]  vote;
reg[2:0]  sum;integer  i;reg  pass;
always @(vote)
begin   sum=0;
    for(i=0;i<=6;i=i+1)	//for语句
        if(vote[i]) sum=sum+1;
    if(sum[2])  pass=1;  	//超过4人赞成，则通过
    else  	  pass=0;
end
endmodule
