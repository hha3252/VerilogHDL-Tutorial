module HEX2BCD(HexVal,BCD10,BCD1);
input[6:0] HexVal;
output reg[3:0] BCD10,BCD1;//BCD10:ʮλ��BCD1����λ

always @(HexVal)
	begin
		BCD10 = HexVal/10;
		BCD1 = HexVal%10;
	end

endmodule