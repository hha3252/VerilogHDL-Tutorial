module MiaoBiao(clk,clrin,pausein,segdata,sel);//Top Module
input clk,clrin,pausein;//Input 50MHz Clocl, reset，pause
output[6:0] segdata;
output[5:0] sel;//
wire clk100Hz,clk150Hz,clk1Hz,clk1_60Hz,clk1_3600Hz;
wire[6:0] cnt100Val,miaoCnt60Val,fenCntVal;
wire[3:0] BCD5,BCD4,BCD3,BCD2,BCD1,BCD0;
wire clr,pause;

switch clrSwitch(clk100Hz,clrin, clr); //switch(clk,keyin,keyout)
switch pauseSwitch(clk100Hz,pausein, pause);

freqDiv myFreq(clk,clk100Hz,clk150Hz);//freqDiv(clk,clk100Hz,clk150Hz);
count100 mycnt100(clk100Hz,clr,pause,clk1Hz,cnt100Val);//count100(clk,clr,pause,clkout,cntVal);
count60  miaoCnt(clk1Hz,clr,pause,clk1_60Hz,miaoCnt60Val);//count60(clk,clr,pause,clkout,cntVal);
count60  fenCnt(clk1_60Hz,clr,pause,clk1_3600Hz,fenCntVal);

//HEX2BCD(HexVal,BCD10,BCD1)
HEX2BCD myHex2BCD_BFM(cnt100Val,BCD5,BCD4);
HEX2BCD myHex2BCD_Miao(miaoCnt60Val,BCD3,BCD2);
HEX2BCD myHex2BCD_Fen(fenCntVal,BCD1,BCD0);

//DisPlay(clk,BCDData5,BCDData4,BCDData3,BCDData2,BCDData1,BCDData0,segData,sel)
DisPlay MyDisp(clk150Hz,BCD5,BCD4,BCD3,BCD2,BCD1,BCD0,segdata,sel);

endmodule
