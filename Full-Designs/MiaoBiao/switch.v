module switch(clk,keyin,keyout);
input clk,keyin;
output reg keyout;
reg cnt;
reg keytmp0,keytmp1;

always @(posedge clk)
	begin
		cnt =cnt+1;
		if (!cnt) keytmp0 = keyin;
		else keytmp1 = keyin;
		if (keytmp0==keytmp1) keyout =keytmp0;
	end
endmodule
