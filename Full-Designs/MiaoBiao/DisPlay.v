module DisPlay(clk,BCDData5,BCDData4,BCDData3,BCDData2,BCDData1,BCDData0,segData,sel);
input clk;//150Hz
input[3:0] BCDData5,BCDData4,BCDData3,BCDData2,BCDData1,BCDData0;
output reg[6:0] segData;
output reg[5:0] sel;

reg[2:0] cnt;
reg[6:0] displayVal;

always @(posedge clk)
	begin
		cnt=cnt+1;
		if (cnt==6) cnt=0;
		
		case (cnt)
			0 : begin displayVal = BCDData0; sel=6'b000001; end
			1 : begin displayVal = BCDData1; sel=6'b000010; end
			2 : begin displayVal = BCDData2; sel=6'b000100; end
			3 : begin displayVal = BCDData3; sel=6'b001000; end
			4 : begin displayVal = BCDData4; sel=6'b010000; end
			5 : begin displayVal = BCDData5; sel=6'b100000; end
			default :begin displayVal = BCDData0; sel=6'b000001;	end
		endcase
	end

always @(displayVal)
	begin
		case(displayVal)   //��case����������
	     4'd0:segData=7'b1111110;
	     4'd1:segData=7'b0110000;
	     4'd2:segData=7'b1101101;
	     4'd3:segData=7'b1111001;
	     4'd4:segData=7'b0110011;
	     4'd5:segData=7'b1011011;
	     4'd6:segData=7'b1011111;
	     4'd7:segData=7'b1110000;
	     4'd8:segData=7'b1111111; 
	     4'd9:segData=7'b1111011;
	     default: segData=7'bx;
	   endcase 
	end

endmodule