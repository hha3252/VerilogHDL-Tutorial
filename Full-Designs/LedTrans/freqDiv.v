module freqDiv(clkin,reset,clkout1,clkout2);
input clkin,reset;
output reg clkout1,clkout2;//

reg[9:0] cnt;
reg[3:0] cnt2;

always @(posedge clkin)
	begin
		if (reset) cnt<=0;
		else
			if (cnt<999) begin cnt<=cnt+1; clkout1<=0; end
			else begin cnt<=0; clkout1<=1; end
	end

always @(posedge clkin)
	begin
		if (reset) cnt2<=0;
		else
			if (cnt2<9) begin cnt2<=cnt2+1; clkout2<=0; end
			else begin cnt2<=0; clkout2<=1; end
	end

endmodule
